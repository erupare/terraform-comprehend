#-----
# MAIN
#-----

output "region" {
  value = "${var.region}"
}

output "account_id" {
  value = "${data.aws_caller_identity.current.account_id}"
}

output "user_id" {
  value = "${data.aws_caller_identity.current.user_id}"
}

output "arn" {
  value = "${data.aws_caller_identity.current.arn}"
}


# TODO: add missing outputs.